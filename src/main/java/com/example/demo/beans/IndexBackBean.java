package com.example.demo.beans;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named(value = "index")
@SessionScoped
@Setter
@Getter
public class IndexBackBean implements Serializable {

    private String initMessage = "Hola Mick";

    @PostConstruct
    public void init(){

    }
}


