package com.example.demo.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@Configuration
//@EnableWebSecurity
public class SecurityConfiguration /*extends WebSecurityConfigurerAdapter*/ {

    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable();
        http.cors().and().csrf().disable();
        http.authorizeRequests()
                .antMatchers("/h2-console/**", "/swagger-resources/**", "/swagger-ui/**", "/swagger-ui.html", "/v2/api-docs").permitAll()
                .anyRequest().authenticated();
        http.headers().frameOptions().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers( "/h2-console/**", "/swagger-resources/**", "/swagger-ui/**", "/swagger-ui.html", "/v2/api-docs");
    }*/
}
