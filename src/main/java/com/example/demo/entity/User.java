package com.example.demo.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "usuario")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "uuidUsers")
    @GenericGenerator(name = "uuidUsers", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Basic
    @NotNull
    @Column(name = "userName", length = 16)
    private String username;
}
